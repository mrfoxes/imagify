import {
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common'
import { Response } from 'express'
import { ConvertDto } from './ConvertDto'
import { ConvertService } from './convert.service'
import { PathLike } from 'fs'

export interface IConvertResponse {
  id: PathLike
}

@Controller('convert')
export class ConvertController {
  constructor(private convertService: ConvertService) {
  }

  @Get(':id')
  /**
   * Get image from conversion id
   */
  async getConverted(
    @Res() response: Response,
    @Param('id') id: string): Promise<Response<BinaryType>> {

    if (!await this.convertService.conversionExist(id)) {
      throw new NotFoundException()
    }

    response.set('Content-Type', this.convertService.getConvertedImageContentType(id))
    response.set('Content-Length', String(this.convertService.getConvertedImageStat(id).size))
    response.set('Content-Disposition', `attachment; filename=${id}`)

    return this.convertService.getConvertedImageStream(id).pipe(response)
  }

  @Post()
  @UsePipes(new ValidationPipe({transform: true}))
  /**
   * Convert image from a given url
   */
  async convert(@Body() body: ConvertDto): Promise<IConvertResponse> {
    try {
      return {
        id: await this.convertService.convertImage(body),
      }
    } catch (e) {
      throw new InternalServerErrorException(e.message)
    }
  }
}
