import { IsNotEmpty, IsString } from 'class-validator'

export class GetConvertedDto {
  @IsString()
  @IsNotEmpty()
  id: string
}
