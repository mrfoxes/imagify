import { IsIn, IsOptional, IsString, IsUrl } from 'class-validator'

export const IMAGE_FIT_FILL = 'fill'
export const IMAGE_FIT_CONTAIN = 'contain'
export const IMAGE_FIT_COVER = 'cover'
export const IMAGE_FIT_NONE = 'none'
export const IMAGE_FIT_SCALE_DOWN = 'scale-down'

export const IMAGE_FORMAT_PNG = 'png'
export const IMAGE_FORMAT_JPEG = 'jpeg'

export class ConvertDto {
  @IsUrl({
    require_tld: false
  })
  imageUrl: string

  @IsIn([IMAGE_FORMAT_PNG, IMAGE_FORMAT_JPEG, null])
  outputFormat?: string = null

  height?: number = null

  width?: number = null

  @IsIn([IMAGE_FIT_FILL, IMAGE_FIT_CONTAIN, IMAGE_FIT_COVER, IMAGE_FIT_NONE, IMAGE_FIT_SCALE_DOWN])
  fit?: string = IMAGE_FIT_COVER
}
