import { Injectable } from '@nestjs/common'
import { ConvertDto } from './ConvertDto'
import * as os from 'os'
import * as https from 'https'
import { IncomingMessage } from 'http'
import * as mime from 'mime-types'
import * as crypto from 'crypto'
import * as fs from 'fs'
import { PathLike, ReadStream, Stats, WriteStream } from 'fs'
import * as path from 'path'
import * as sharp from 'sharp'
import { IConvertService } from './IConvertService'
const url = require('url')

const request = (() => {
  const url = require('url'),
    adapters = {
      'http:': require('http'),
      'https:': require('https'),
    };

  return function(inputUrl) {
    return adapters[url.parse(inputUrl).protocol]
  }
})()

@Injectable()
export class ConvertService implements IConvertService {
  private static getTempDir(): PathLike {
    return os.tmpdir()
  }

  static getFileName(contentType: string, imageUrl: string) {
    const fileExtension: string = mime.extension(mime.lookup(url.parse(imageUrl).pathname))

    const fileName: string = crypto.createHash('md5').update(imageUrl).digest('hex')

    return `${fileName}.${fileExtension}`
  }

  /**
   * Download a file into a system temp folder from a given HTTPS url then return the path
   * @param imageUrl
   */
  static downloadImage = (imageUrl: string): Promise<PathLike> => {
    return new Promise<PathLike>((resolve, reject) => {
      const tmpDir: PathLike = ConvertService.getTempDir()

      request(imageUrl).get(imageUrl, (res: IncomingMessage) => {
        const fullFileName: string = ConvertService.getFileName(res.headers['content-type'], imageUrl)

        const downloadFilePath: string = path.join(String(tmpDir), fullFileName)

        const file: WriteStream = fs.createWriteStream(downloadFilePath)

        if (res.statusCode === 200) {
          res.pipe(file)

          file.on('close', () => resolve(file.path))
        } else {
          reject()
        }
      })
    })
  }

  /**
   * Process the image at a given file path
   * @param filePath
   */
  private static async processImageJPEG(filePath: PathLike): Promise<PathLike> {
    const tmpDir: PathLike = ConvertService.getTempDir()
    const originalFileNameList = String(filePath).split('/').reverse()[0].split('.')
    const convertedFileName = [
      originalFileNameList.splice(0, originalFileNameList.length - 1),
      'converted',
      originalFileNameList.splice(originalFileNameList.length - 1, originalFileNameList.length),
    ].join('.')

    const outputFilePath = path.join(String(tmpDir), convertedFileName)

    return new Promise(async (resolve, reject) => {
      try {
        await sharp(filePath)
          .resize(140, 140, {
            kernel: sharp.kernel.nearest,
            fit: 'cover',
            position: 'center',
          })
          .jpeg({
            quality: 60,
          })
          .toFile(outputFilePath)


        resolve(outputFilePath)
      } catch (e) {
        reject(e.message)
      }
    })
  }

  private static getProcessor(outputFormat?: string | undefined) {
    const processors = {
      'default': ConvertService.processImageJPEG,
      'format_jpeg': ConvertService.processImageJPEG,
    }

    return (processors[`format_${outputFormat}`] || processors['default'])
  }

  /**
   * Convert image from url respect to given options
   * @param imageData
   */
  public async convertImage(imageData: ConvertDto): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const filePath: PathLike = await ConvertService.downloadImage(imageData.imageUrl)

        const processedImagePath: PathLike = await ConvertService.getProcessor(imageData.outputFormat)(filePath)

        resolve(String(processedImagePath).split('/').reverse()[0])
      } catch (e) {
        reject(e)
      }
    })
  }

  public getImagePath(fileName: string): string {
    return path.join(String(ConvertService.getTempDir()), fileName)
  }

  public async conversionExist(imageId: string): Promise<boolean> {
    return new Promise(resolve => {
      fs.access(this.getImagePath(imageId), fs.constants.F_OK, (err) => {
        if (err) {
          return resolve(false)
        }

        return resolve(true)
      })
    })
  }

  public getConvertedImageStream(imageId: string): ReadStream {
    return fs.createReadStream(this.getImagePath(imageId))
  }

  public getConvertedImageContentType(imageId: string): string {
    return mime.contentType(path.extname(this.getImagePath(imageId)))
  }

  public getConvertedImageStat(imageId: string): Stats {
    return fs.statSync(this.getImagePath(imageId))
  }
}
