import { ConvertDto } from './ConvertDto'
import { ReadStream, Stats } from 'fs'

export interface IConvertService {
  convertImage(imageData: ConvertDto): Promise<string>
  conversionExist(imageId: string): Promise<boolean>
  getConvertedImageStat(imageId: string): Stats
  getConvertedImageStream(imageId: string): ReadStream
  getConvertedImageContentType(imageId: string): string
}
