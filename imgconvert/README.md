# Image converter

A service that accept as input an image url and convert / optimize it.

The converted image can be downloaded back by the requesting client.

The service expose two endpoints

    POST /convert
    GET /convert/:id
    
## Examples

Convert image

    http post http://localhost:3000/convert imageUrl=https://demo-res.cloudinary.com/image/upload/sample.jpg
    
Download converted

     http get http://localhost:3000/convert/ff4058d629bad485f92f0cdc119b0389.converted.jpeg

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
