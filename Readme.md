# Imagify

Three main services

- Web client
- Core
- Imgconvert service

Concept
-------
You create a post from the web client.
The core service accept the user request and send that post into a job queue managed via @bull brokered via redis.
The job that work the post call the imgconvert service thet optimize the image.
Post on the wall.
-------

## Run

Add minio to your host file in order to resolve images url correctly
    
    sudo vi /etc/hosts
    
Add entries

    127.0.0.1 minio
    localhost minio
    
## Start services

#### IMPORTANT

All those ports must be free in order for all to start correctly

- 4000
- 3000
- 3010
- 6397
- 27017
- 9000

Thank run
    
    docker-compose up -d

- Web at http://localhost:4000
- Core api at http://localhost:3000
    
    Endpoints: (authenticated via the custom header Imagify-Username)
    - GET /wall
    - POST /wall
- Imgconvert service api at http://localhost:3010
    
    Endpoints:
    - GET /convert/{id}
    - POST /convert
- Redis at http://localhost:6379
- Mongo at http://localhost:27017
- Minio at http://localhost:9000


### ATTENZIONE

If you don't see the image immediately into the feed when you submit the form it's obviously because the queue it's running and the job isn't finished.

In a real scenario you will have some sorts of subscriptions that inject the post into the feed and other kind of mechanism in place.    
