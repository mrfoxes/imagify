import React, { createContext, Dispatch, FunctionComponent, useReducer } from 'react'

type SessionState = {
  loggedIn: boolean
  username: string
}

type SessionStartPayload = {
  username: string
}

export type SessionAction =
  | { type: 'START', payload: SessionStartPayload }
  | { type: 'END' }


export enum SessionTypes {
  Start = 'START',
  End = 'END',
}

const initialState: SessionState = {
  loggedIn: false,
  username: '',
}

export type ISessionContext = { state: SessionState, dispatch: Dispatch<SessionAction> }

const sessionContext = createContext<ISessionContext>({
  state: initialState,
  dispatch: () => null,
})
const {Provider} = sessionContext

const sessionReducer = (state: SessionState, action: SessionAction) => {
  switch (action.type) {
    case 'START':
      return {
        ...state,
        loggedIn: true,
        username: action.payload.username,
      }
    case 'END':
      return initialState
    default:
      return state
  }
}

interface ISessionProvider {
}

const SessionProvider: FunctionComponent<ISessionProvider> = ({children}) => {
  const [state, dispatch] = useReducer(sessionReducer, initialState)

  return (
    <Provider value={{state, dispatch}}>
      {children}
    </Provider>
  )
}

export default SessionProvider
export {
  sessionContext,
}
