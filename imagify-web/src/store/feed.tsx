import React, { createContext, Dispatch, FunctionComponent, useReducer } from 'react'

type FeedState = {
  load: boolean
}

export type FeedAction =
  | { type: 'LOAD_START' }
  | { type: 'LOAD_STOP' }


export enum FeedTypes {
  LoadStart = 'LOAD_START',
  LoadStop = 'LOAD_STOP'
}

const initialState: FeedState = {
  load: true,
}

export type IFeedContext = { state: FeedState, dispatch: Dispatch<FeedAction> }

const feedContext = createContext<IFeedContext>({
  state: initialState,
  dispatch: () => null,
})
const {Provider} = feedContext

const feedReducer = (state: FeedState, action: FeedAction) => {
  switch (action.type) {
    case 'LOAD_START':
      return {
        ...state,
        load: true
      }
    case 'LOAD_STOP':
      return {
        ...state,
        load: false
      }
    default:
      return state
  }
}

interface IFeedProvider {
}

const FeedProvider: FunctionComponent<IFeedProvider> = ({children}) => {
  // @ts-ignore
  const [state, dispatch] = useReducer(feedReducer, initialState)

  return (
    <Provider value={{state, dispatch}}>
      {children}
    </Provider>
  )
}

export default FeedProvider
export {
  feedContext,
}
