import React, { FunctionComponent, useContext } from 'react'
import { sessionContext } from '../store/session'
import Box from 'ui-box'
import Feed from './Feed/Feed'
import WritePost from './Feed/WritePost'

interface IHomeProps {
}

const Home: FunctionComponent<IHomeProps> = (props) => {
  const {state: sessionState} = useContext(sessionContext)

  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
    >
      <Box
        display="flex"
        flexDirection="column"
        backgroundColor="lightgrey"
        border="1px solid #696969"
        padding={20}
        width="100%"
      >
        <Box>
          Welcome {sessionState.username}
        </Box>
        <Box marginTop={20}>
          <WritePost />
        </Box>
      </Box>
      <Box flex={1} height="100%">
        <Box>
          <Feed />
        </Box>
      </Box>
    </Box>
  )
}

export default Home
