import React, { useContext } from 'react'
import './App.css'
import { ISessionContext, sessionContext } from '../store/session'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { authenticatedRoutes, unauthenticatedRoutes } from '../config/router'

function App() {
  const {state: sessionState} = useContext<ISessionContext>(sessionContext)

  return (
    <BrowserRouter>
      {!sessionState.loggedIn && renderRoutes(unauthenticatedRoutes)}
      {sessionState.loggedIn && renderRoutes(authenticatedRoutes)}
    </BrowserRouter>
  )
}

export default App
