import React, { FunctionComponent } from 'react'
import Box from 'ui-box'

interface IImageUploadPreview {
  image: File
}

const ImageUploadPreview: FunctionComponent<IImageUploadPreview> = (props) => {
  const {
    image,
  } = props

  return (
    <Box
      display="flex"
      height={140}
      width={140}
      justifyContent="center"
      alignItems="center"
      backgroundImage={`url('${URL.createObjectURL(image)}')`}
      backgroundSize="cover"
    />
  )
}

export default ImageUploadPreview
