import React, { FunctionComponent } from 'react'
import Box from 'ui-box'

interface IProps {
  image: any
}

const Image: FunctionComponent<IProps> = (props) => {
  const {
    image,
  } = props

  return (
    <Box backgroundColor="lightyellow" marginTop={20}>
      {image.isPosting && (
        <Box>
          Publishing in progress....
        </Box>
      )}
      {image?.instant?.url && (
        <Box border="1px solid grey" padding={10}>
          <Box>
            <Box>
              @ {image.owner}
            </Box>
            <Box marginTop={4} marginBottom={8}>
              {new Intl.DateTimeFormat(navigator.language).format(new Date(image.createdAt))}
            </Box>
          </Box>
          <Box>
            <img src={image.instant.url} alt="" width="100%" height="100%" />
          </Box>
        </Box>
      )}
    </Box>
  )
}

export default Image
