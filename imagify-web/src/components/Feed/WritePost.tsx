import React, { ChangeEvent, useContext, useState } from 'react'
import Box from 'ui-box'
import './WritePost.css'
import ImageUploadPreview from './ImageUploadPreview'
import ImagifyApi from '../../libs/api'
import { sessionContext } from '../../store/session'
import { feedContext, FeedTypes } from '../../store/feed'

const MAX_UPLOAD = 1

const WritePost = () => {
  const {state: sessionContextState} = useContext(sessionContext)
  const {dispatch: feedContextDispatch} = useContext(feedContext)
  const [images, setImages] = useState<Array<any>>([])

  const onImageChange = (event: ChangeEvent<HTMLInputElement>) => {
    const files: FileList | null = event.target.files

    if (files !== null && files.length > 0) {
      setImages([...images.splice(0, MAX_UPLOAD - 1), files[0]])
    }
  }

  const resetForm = async () => {
    setImages([])
  }

  const uploadImages = () => {
    var formdata = new FormData()
    formdata.append('image', images[0])

    ImagifyApi
      .createPost(sessionContextState.username, formdata)
      .then(data => {
        resetForm()
        feedContextDispatch({
          type: FeedTypes.LoadStart
        })
      })
      .catch(e => {
        console.log(e)
      })
  }

  return (
    <Box>
      {images.length > 0 && (
        <Box display="flex" marginBottom={10}>
          {images.map((image: File) => {
            return (
              <Box marginRight={5}>
                <ImageUploadPreview image={image} />
              </Box>
            )
          })}
        </Box>
      )}
      <Box>
        <div className="image-input">
          <label htmlFor="image-input">
            Choose image
          </label>
          <input type="file" id="image-input" accept="image/jpeg|image/png" onChange={onImageChange} />
        </div>
      </Box>
      <Box marginTop={10}>
        <button type="submit" onClick={uploadImages}
                disabled={(images.length < MAX_UPLOAD || images.length > MAX_UPLOAD)}>
          Create post
        </button>
      </Box>
    </Box>
  )
}

export default WritePost
