import React, { FunctionComponent, useContext, useEffect, useState } from 'react'
import Box from 'ui-box'
import Image from './Post/Image'
import { sessionContext } from '../../store/session'
import ImagifyApi from '../../libs/api'
import { feedContext, FeedTypes } from '../../store/feed'

interface IFeedProps {

}

const Feed: FunctionComponent<IFeedProps> = props => {
  const {state: sessionContextState} = useContext(sessionContext)
  const {state: feedContextState, dispatch: feedContextDispatch} = useContext(feedContext)

  const [feed, setFeed] = useState([])
  const [isLoadingFeed, setIsLoadingFeed] = useState(true)

  const fetchWall = async () => {
    feedContextDispatch({
      type: FeedTypes.LoadStart,
    })

    const response = await ImagifyApi.getWall(sessionContextState.username)

    feedContextDispatch({
      type: FeedTypes.LoadStop,
    })

    setFeed(response)
  }

  useEffect(() => {
    if (feedContextState.load) {
      setTimeout(() => {
        fetchWall()
      }, 500)
    }
  }, [feedContextState.load])

  const isWallEmpty = () => {
    return feed.length === 0 && !feedContextState.load
  }

  const hasPendingPosts = () => {
    return feed.filter((feedImage: any) => {
      return feedImage.isPosting
    }).length > 0
  }

  // Just for sample to reload the feed if there a job in progress
  if(feed.length > 0) {
    if (hasPendingPosts()) {
      setTimeout(() => {
        fetchWall()
      }, 2000)
    }
  }

  return (
    <Box margin="auto" maxWidth={500} display="flex" flexDirection="column" alignItems="center">
      {(feedContextState.load && !hasPendingPosts()) && (
        <Box>
          Loading...
        </Box>
      )}
      {isWallEmpty() && (
        <Box>
          Wall empty. <br />
          Create your first post
        </Box>
      )}
      {feed.map((image: any) => {
        return (
          <Box marginBottom={20} key={image._id}>
            <Image image={image} />
          </Box>
        )
      })}
    </Box>
  )
}

export default Feed
