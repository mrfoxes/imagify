import React, { ChangeEvent, FunctionComponent, useContext, useState } from 'react'
import Box from 'ui-box'
import { ISessionContext, sessionContext, SessionTypes } from '../store/session'

interface ILogin {
}

const Login: FunctionComponent<ILogin> = (props) => {
  const [username, setUsername] = useState('')
  const {dispatch: sessionDispatch} = useContext<ISessionContext>(sessionContext)

  const start = (inputUsername: string) => {
    sessionDispatch({
      type: SessionTypes.Start,
      payload: {
        username: inputUsername,
      },
    })
  }

  // To auto login and skip initial setup
  // start('mario')

  return (
    <Box
      display="flex"
      position="absolute"
      height="100%"
      width="100%"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Box>
        <h1>
          Welcome to Imagify
        </h1>
        <p>
          You're just one step closer...
        </p>
        <p>
          Tell me something about Yourself
        </p>
      </Box>
      <form action="" onSubmit={event => event.preventDefault()}>
        <Box display="flex" flexDirection="column">
          <Box>
            <input
              required={true}
              type="text"
              placeholder="username"
              autoFocus={true}
              onChange={(event: ChangeEvent<HTMLInputElement>) => {
                setUsername(event.target.value)
              }}
            />
          </Box>
          <Box display="flex" justifyContent="center" marginTop={10}>
            <button type="submit" onClick={() => start(username)}>Start</button>
          </Box>
        </Box>
      </form>
    </Box>
  )
}

export default Login

