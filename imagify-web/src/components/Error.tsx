import React, { FunctionComponent } from 'react'
import Box from 'ui-box'

interface IError {
  message: string
}

const Error: FunctionComponent<IError> = (props) => {
  const {
    message,
  } = props

  return (
    <Box>
      {message}
    </Box>
  )
}

export default Error
