class Config {
  private _config: any

  set config(value: any) {
    this._config = value
  }

  get(key?: string): any | undefined {
    if (key) {
      return this._config[key]
    }

    return this._config
  }
}

const AppConfig = new Config()

export default AppConfig
