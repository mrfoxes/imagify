import { RouteConfig } from 'react-router-config'
import { Redirect } from 'react-router-dom'
import React from 'react'

const Login = React.lazy(() => import('../components/Login'))
const Home = React.lazy(() => import('../components/Home'))

const unauthenticatedRoutes: Array<RouteConfig> = [
  {
    component: Login,
    path: '/login',
    exact: true,
  },
  {
    component: () => <Redirect to="/login" />,
    path: '/',
    exact: false,
  },
]

const authenticatedRoutes: Array<RouteConfig> = [
  {
    component: Home,
    path: '/',
    exact: false,
  },
]

export {
  unauthenticatedRoutes,
  authenticatedRoutes,
}
