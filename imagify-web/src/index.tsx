import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import AppConfig from './config/config'
import ImagifyApi from './libs/api'

const App = React.lazy(() => import('./components/App'))
const SessionProvider = React.lazy(() => import('./store/session'))
const FeedProvider = React.lazy(() => import('./store/feed'))
const Error = React.lazy(() => import('./components/Error'))

async function loadConfig() {
  return new Promise(resolve => {
    resolve({
      imagifyApiUrl: 'http://localhost:3000',
    })
  })
}

async function bootstrap() {
  const rootEl: HTMLElement | null = document.getElementById('root')

  try {
    AppConfig.config = await loadConfig()
  } catch (e) {
    return ReactDOM.render(<Error message="Error loading config" />, rootEl)
  }

  ImagifyApi.init({
    baseUrl: AppConfig.get('imagifyApiUrl')
  })

  return ReactDOM.render(
    <React.StrictMode>
      <Suspense fallback={<div>Loading...</div>}>
        <SessionProvider>
          <FeedProvider>
            <App />
          </FeedProvider>
        </SessionProvider>
      </Suspense>
    </React.StrictMode>,
    rootEl,
  )
}

bootstrap()
