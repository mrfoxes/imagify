export interface IImagifyApiCOnfig {
  baseUrl?: string
}

class ApiClass {
  private baseUrl: string | '' = ''

  init(config: IImagifyApiCOnfig) {
    this.baseUrl = config?.baseUrl || ''
  }

  async createPost(user: string, body: any) {
    const response = await fetch(`${this.baseUrl}/wall/`, {
      method: 'POST',
      headers: {
        'Imagify-Username': user,
      },
      body
    })

    return await response.json()
  }

  async getWall(user: string) {
    const response = await fetch(`${this.baseUrl}/wall/`, {
      headers: {
        'Content-Type': 'application/json',
        'Imagify-Username': user,
      },
    })

    return await response.json()
  }
}

const ImagifyApi = new ApiClass()

export default ImagifyApi
