import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { client } from './minio'


async function initMinio() {
  client.bucketExists('imagify', async (err, exists) => {
    if (!exists) {
      await client.makeBucket('imagify', 'us-east-1')
    }
  })
}

async function bootstrap() {
  await initMinio()
  const app = await NestFactory.create(AppModule)
  app.enableCors()
  await app.listen(process.env.PORT || 3000)
}

bootstrap()
