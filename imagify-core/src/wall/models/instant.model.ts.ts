import { Document } from 'mongoose'
import { IsDate, IsMongoId } from 'class-validator'

export class InstantModel extends Document {
  @IsMongoId()
  readonly id: string

  @IsDate()
  createdAt: Date;

  @IsDate()
  updatedAt: Date;
}
