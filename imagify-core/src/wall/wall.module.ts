import { Module } from '@nestjs/common'
import { WallController } from './wall.controller'
import { WallService } from './wall.service'
import { MongooseModule } from '@nestjs/mongoose'
import { InstantSchema } from './wall.schema'
import { AuthService } from '../auth/auth.service'
import { BullModule } from '@nestjs/bull'
import { InstantsConsumer } from './wall.consumers'
import { OptimizeService } from './services/optimize.service'
import { StorageService } from '../storage/storage.service'
import { ImgconvertService } from './services/imgconvert.service'

@Module({
  imports: [
    MongooseModule.forFeature([{
      name: 'Instant', schema: InstantSchema,
    }]),
    BullModule.registerQueue({
      name: 'instants',
      redis: {
        host: process.env.REDIS_URL || 'localhost',
        port: 6379,
      },
    }),
  ],
  controllers: [WallController],
  providers: [WallService, AuthService, InstantsConsumer, OptimizeService, StorageService, ImgconvertService],
})
export class WallModule {
}
