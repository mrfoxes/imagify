import { Injectable } from '@nestjs/common'
import { ImgconvertService, IOptimize } from './imgconvert.service'
import { StorageService } from '../../storage/storage.service'
import * as path from 'path'

@Injectable()
export class OptimizeService {
  constructor(
    private imgconvertService: ImgconvertService,
    private storageService: StorageService,
  ) {
  }

  async optimize(user: string, imageUrl: URL): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        const optimizeData: IOptimize = await this.imgconvertService.optimize(imageUrl)
        const convertedImageBufferResponse: Buffer = await this.imgconvertService.downloadConverted(optimizeData.id)

        const imageKey = path.join('media', user, optimizeData.id)

        await this.storageService.upload(path.join(imageKey), convertedImageBufferResponse)

        resolve(imageKey)
      } catch (e) {
        reject(e)
      }
    })
  }
}
