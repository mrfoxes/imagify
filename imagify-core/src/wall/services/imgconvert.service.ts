import { Injectable } from '@nestjs/common'
import { IncomingMessage } from 'http'
import * as path from "path"
import { WriteStream } from "fs"
import * as fs from "fs"
import * as os from 'os'

const url = require('url')

const request = (() => {
  const adapters = {
    'http:': require('http'),
    'https:': require('https'),
  }

  return function (inputUrl) {
    return adapters[url.parse(inputUrl).protocol]
  }
})()

export interface IOptimize {
  id: string
}

@Injectable()
export class ImgconvertService {
  private imgconvertUrl: URL = url.parse(process.env.IMGCONVERT_URL || 'http://localhost:3010')

  async downloadConverted(id: string): Promise<Buffer> {
    return new Promise<Buffer>((resolve, reject) => {
      request(this.imgconvertUrl.protocol).get(path.join(this.imgconvertUrl.href, `/convert/${id}`), (res: IncomingMessage) => {
        let chunks = [];

        res.on('data', (chunk) => {
          chunks.push(Buffer.from(chunk, 'binary'));
        });

        res.on('end', () => {
          if (res.statusCode === 200) {
            return resolve(Buffer.concat(chunks))
          }

          return reject(res.statusCode)
        });

        res.on('error', err => {
          reject(err)
        })
      })
    })
  }

  async optimize(imageUrl: URL): Promise<IOptimize> {
    return new Promise<IOptimize>((resolve, reject) => {
      const data = JSON.stringify({
        imageUrl: imageUrl.href,
      })

      const options = {
        protocol: this.imgconvertUrl.protocol,
        hostname: this.imgconvertUrl.hostname,
        port: this.imgconvertUrl.port,
        path: '/convert',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': data.length,
        },
      }


      const req = request(imageUrl.protocol)
        .request(options, (res) => {
          let data = ''

          res.on('data', (chunk) => {
            data += chunk
          })

          res.on('end', () => {
            resolve(JSON.parse(data))
          })

        })
        .on('error', (err) => {
          reject(err)
        })

      req.write(data)
      req.end()
    })
  }
}
