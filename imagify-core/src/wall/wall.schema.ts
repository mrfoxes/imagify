import * as mongoose from 'mongoose'

export const InstantSchema = new mongoose.Schema({
  image: String,
  owner: {
    type: String,
    required: true
  },
  isPosting : {
    type: Boolean,
    default: true
  },
  instant: {
    type: Object,
    required: false,
    default: null,
  },
  public: {
    type: Boolean,
    default: true
  },
  postedAt:{
    type: Date,
    default: null
  }
}, {
  timestamps: true,
  skipVersioning: true,
  versionKey: false
});
