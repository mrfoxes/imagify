import { OnQueueActive, Process, Processor } from '@nestjs/bull'
import { Job } from 'bull'
import { OptimizeService } from './services/optimize.service'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { InstantModel } from './models/instant.model.ts'

const url = require('url')

export interface IOptimizeJobData {
  _id: string,
  user: string,
  uploadedImageUrl: string
}

@Processor('instants')
export class InstantsConsumer {
  constructor(
    @InjectModel('Instant') private instantModel: Model<InstantModel>,
    private optimizeService: OptimizeService,
  ) {
  }

  @Process('optimize')
  async optimize(job: Job<IOptimizeJobData>) {
    const {
      data,
    } = job

    try {
      const optimizedImageKey = await this.optimizeService.optimize(
        data.user,
        url.parse(data.uploadedImageUrl)
      )

      await this.instantModel.updateOne({
        _id: data._id,
      }, {
        $set: {
          isPosting: false,
          instant: {
            key: optimizedImageKey,
            metadata: {
              key: 'value',
            },
          },
          postedAt: new Date(),
        },
      })
    } catch (e) {
      await this.instantModel.updateOne({
        _id: data._id,
      }, {
        $set: {
          error: {
            message: 'Cannot process post at the moment',
          },
        },
      })

      // Retry job otherwise discard post
    }
  }

  @OnQueueActive()
  onActive(job: Job) {
    console.log(
      `Processing job ${job.id} of type ${job.name} with data ${JSON.stringify(job.data)}...`,
    )
  }
}
