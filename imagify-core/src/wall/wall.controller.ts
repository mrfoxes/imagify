import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common'
import { InstantModel } from './models/instant.model.ts'
import { WallService } from './wall.service'
import { AuthService } from '../auth/auth.service'
import { Request, Response } from 'express'
import { InjectQueue } from '@nestjs/bull'
import { Queue } from 'bull'
import CreateInstantDtoModel from './models/createinstantdto.model'
import { FileInterceptor } from '@nestjs/platform-express'
import { IFileUpload } from '../storage/fileupload.interface'
import { StorageService } from '../storage/storage.service'

@Controller('wall')
export class WallController {
  constructor(
    private wallService: WallService,
    private authService: AuthService,
    private storageService: StorageService,
    @InjectQueue('instants') private instantsQueue: Queue,
  ) {
  }

  @Get()
  async getWall(
    @Req() request: Request,
  ): Promise<any> {
    const user = this.authService.getUserFromRequest(request)

    return await this.wallService.getInstantsWallForCurrentUser(user)
  }

  @Post()
  @UsePipes(new ValidationPipe({transform: true}))
  @UseInterceptors(FileInterceptor('image'))
  async createInstant(
    @Res() response: Response,
    @Req() request: Request,
    @UploadedFile() image: IFileUpload,
    @Body() createInstantDto: CreateInstantDtoModel,
  ): Promise<Response<any>> {
    if (!image) {
      throw new BadRequestException(['image is required'], 'Validation Failed')
    }

    const user = this.authService.getUserFromRequest(request)

    try {
      const fileKey: string = `tmp/${user}/${image.originalname}`
      await this.storageService.upload(fileKey, image.buffer)
      const instant: InstantModel = await this.wallService.createInstant(user, {})

      await this.instantsQueue.add('optimize', {
        _id: instant.id,
        user,
        uploadedImageUrl: await this.storageService.getFileUrl(fileKey),
      }, {
        delay: 0,
      })

      response.status(HttpStatus.ACCEPTED)
      return response.send(instant)
    } catch (e) {
      throw new InternalServerErrorException(e.message)
    }
  }
}
