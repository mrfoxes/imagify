import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { InstantModel } from './models/instant.model.ts'
import { StorageService } from '../storage/storage.service'

@Injectable()
export class WallService {
  constructor(
    private storageService: StorageService,
    @InjectModel('Instant') private instantModel: Model<InstantModel>) {
  }

  async getInstantsWallForCurrentUser(currentUser?: string): Promise<any[]> {
    const ctx = this

    return new Promise(async (resolve) => {
      const wallData = await this.instantModel
        .find({})
        .sort({
          createdAt: -1,
        })
        .exec()

      const mapInstant = async item => {
        async function getInstantData(instantData: any = undefined): Promise<any> {
          if (instantData) {
            return {
              instant: {
                url: await ctx.storageService.getFileUrl(instantData?.key || null),
                ...instantData,
              },
            }
          }

          return {}
        }

        return Promise.resolve({
          ...item.toJSON(),
          ...await getInstantData(item.instant),
        })
      }

      const getData = async () => {
        return Promise.all(wallData.map(item => mapInstant(item)))
      }

      getData()
        .then(data => {
          resolve(data)
        })
    })
  }

  async createInstant(owner: string, data: any = {}) {
    return new this.instantModel({
      owner,
      ...data,
    }).save()
  }
}
