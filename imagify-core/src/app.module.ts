import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { WallModule } from './wall/wall.module'
import { MongooseModule } from '@nestjs/mongoose'
import { AutoStrategyMiddleware } from './auth/imagify.strategy'
import { AuthService } from './auth/auth.service'

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URL || 'mongodb://root:password@localhost/admin'),
    WallModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer
      .apply(AutoStrategyMiddleware)
      .forRoutes('wall')
  }
}
