export interface IStorageProvider {
  upload: Function
  getFileUrl: Function
}
