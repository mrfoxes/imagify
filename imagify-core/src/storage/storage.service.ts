import { Injectable } from '@nestjs/common'
import { IStorageProvider } from './storage.interface'
import { client } from '../minio'

@Injectable()
export class StorageService implements IStorageProvider {
  upload(key: string, imageBuffer: Buffer): Promise<any> {
    return new Promise((resolve, reject) => {
      client
        .putObject(
          'imagify',
          key,
          imageBuffer,
          (err, result) => {
            if (err) return reject(err)

            resolve(result)
          })
    })
  }

  getFileUrl(fileName: string): Promise<string> {
    if(fileName === null) {
      return null
    }

    return new Promise<string>((resolve, reject) => {
      client
        .presignedUrl(
          'GET',
          'imagify',
          fileName,
          60 * 60,
          (err, presignedUrl) => {
            if (err) return reject(err)

            return resolve(presignedUrl)
          })
    })
  }
}
