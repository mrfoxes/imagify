import { Injectable } from '@nestjs/common'
import { Request } from 'express'

@Injectable()
export class AuthService {
  async validateUser(username: string): Promise<boolean> {
    return new Promise(resolve => {
      username !== undefined && username !== '' ? resolve(true) : resolve(false)
    })
  }

  getUserFromRequest(req: Request): string {
    return String(req.headers['imagify-username'])
  }
}
