import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common'
import { AuthService } from './auth.service'
import { Request } from 'express'

@Injectable()
export class AutoStrategyMiddleware implements NestMiddleware {
  constructor(private authService: AuthService) {
  }

  async validate(req): Promise<boolean> {
    return new Promise(async resolve => {
      resolve(await this.authService.validateUser(req.headers['imagify-username']))
    })
  }

  async use(req: Request, res: any, next: () => void): Promise<any> {
    if (await this.validate(req)) {
      return next()
    }

    throw new UnauthorizedException()
  }
}
